using Dal.EF.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Dal.EF
{
    public class FirstAppContextFactory : IDesignTimeDbContextFactory<FirstAppContext>
    {
        public FirstAppContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<FirstAppContext>();
            optionsBuilder.UseNpgsql("Data Source=blog.db");

            return new FirstAppContext(optionsBuilder.Options);
        }
    }
}