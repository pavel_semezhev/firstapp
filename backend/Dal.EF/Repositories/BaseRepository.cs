using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Domain.Interfaces;
using Dal.EF.Contexts;

namespace Dal.EF.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity: class
    {
        #region properties and fields

        /// <summary>
        /// Контекст БД
        /// </summary>
        protected readonly FirstAppContext Context;

        /// <summary>
        /// Флаг уничтожения класса
        /// </summary>
        private bool _disposed = false;

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="context">db context</param>
        public BaseRepository(FirstAppContext context)
        {
            this.Context = context;
        }

        #endregion

        #region IBaseRepository

        /// <summary>
        /// Получение всех объектов указанного типа из БД
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAll()
        {
            return Context.Set<TEntity>().ToList();
        }

        /// <summary>
        /// Получение объекта указанного типа по Id из БД
        /// </summary>
        /// <returns></returns>
        public virtual TEntity GetById(int id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        /// <summary>
        /// Добавление объекта указанного типа в БД
        /// </summary>
        /// <param name="item"></param>
        public void Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
        }

        /// <summary>
        /// Добавление списка объектов указанного типа в БД
        /// </summary>
        /// <param name="entities"></param>
        public void AddRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().AddRange(entities);            
        }

        /// <summary>
        /// Изменение объекта указанного типа в БД
        /// </summary>
        /// <param name="item"></param>
        public void Update(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Сохранение изменений в БД
        /// </summary>
        /// <returns></returns>
        public int Save()
        {            
            return Context.SaveChanges();
        }

        /// <summary>
        /// Удаление записи из БД
        /// </summary>
        /// <param name="entity"></param>
        public void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        #endregion

        #region IDisposable

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}