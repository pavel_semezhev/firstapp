using Dal.EF.Contexts;
using Domain.Entity;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Dal.EF.Repositories
{
    /// <summary>
    /// Repository to work with game objects in db
    /// </summary>
    public class AccountRepository : BaseRepository<Account>, IAccountRepository        
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">db context</param>
        public AccountRepository(FirstAppContext context) : base(context)
        {
        }

        #endregion

        #region IAccountRepository

       /// <summary>
       /// Получение объекта Аккаунта по Login
       /// </summary>
       /// <returns></returns>
       public Account GetByLogin(string login)
       {
           var account = Context.Accounts.FirstOrDefault(acc => acc.Login.Equals(login));            
           return account;
       }

       /// <summary>
       /// Получение объекта Аккаунта по Email
       /// </summary>
       /// <returns></returns>
       public Account GetByEmail(string email)
       {
           var account = Context.Accounts.FirstOrDefault(acc => acc.Email.Equals(email));
           return account;
       }

        #endregion
    }
}
