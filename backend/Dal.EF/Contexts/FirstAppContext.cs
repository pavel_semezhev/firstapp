using Microsoft.EntityFrameworkCore;
using Domain;
using Domain.Entity;
using Dal.EF.Configurations;

namespace Dal.EF.Contexts
{
    public class FirstAppContext : DbContext
    {
        public FirstAppContext(DbContextOptions<FirstAppContext> options) : base(options)
        {            
        }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new AccountConfiguration());
        }

        public DbSet<Account> Accounts
        {
            get;
            set; 
        }

    }
}
