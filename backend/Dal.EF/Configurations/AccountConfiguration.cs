using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dal.EF.Configurations
{
    public class AccountConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder) 
        {
            builder.ToTable("Accounts");
            builder
                .Property(prop => prop.Login)
                .IsRequired();

            builder
                .Property(prop => prop.Email)
                .IsRequired();

            builder
                .Property(prop => prop.Password)
                .IsRequired();
        }
    }
}