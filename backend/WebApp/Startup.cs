using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Dal.EF.Contexts;
using WebApp.Containers;
using Domain.Authentication;
using AutoMapper;



namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {       
            services.AddDbContext(Configuration); 
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers();
            DalEfContainer.RegisterRepositories(services);      
            ServicesContainer.RegisterServices(services);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidIssuer = AuthOptions.ISSUER,
                            ValidateAudience = true,
                            ValidAudience = AuthOptions.AUDIENCE,
                            ValidateLifetime = true,
                            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                            ValidateIssuerSigningKey = true,
                        };
                    });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", 
                                    new OpenApiInfo 
                                    {
                                         Title = "FirstApp", 
                                         Version = "v1",
                                         Description = "Simple api for FirstApp"
                                    });
            });
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
             
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("./swagger/v1/swagger.json", "FirstApp V1");
                c.RoutePrefix = string.Empty;
            });
        }
    }

    internal static class ServiceExtensions
    {
        public static void AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {   
            var connectionStringName = configuration["UseConnectionString"];
            if (string.IsNullOrEmpty(connectionStringName))
            {
                throw new Exception("UseConnectionString value can't be empty or null. Please check configuration file.");
            }
            var connectionString = configuration.GetConnectionString(connectionStringName);
            if (string.Equals(connectionStringName, "InMemory", StringComparison.OrdinalIgnoreCase))
            {
                services.AddDbContext<FirstAppContext>(
                    options =>
                    {
                        options.UseInMemoryDatabase("InMemoryDbContext");
                    }
                );
            }
            else
            {
                services.AddDbContext<FirstAppContext>(
                    options =>
                    {
                        options.UseNpgsql(connectionString);
                    }
                );
            }
        }
    }

}
