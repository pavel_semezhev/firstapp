using Microsoft.AspNetCore.Mvc;
using WebApp.Dto;
using Domain.Entity;
using AutoMapper;
using Domain.Services.Interfaces;
using System;
using Domain.Exceptions;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{

    /// <summary>
    /// Контроллер для работы с аккаунтами
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {

        #region properties and fields

        /// <summary>
        /// Сервис для работы с аккаунтами
        /// </summary>
        private readonly IAccountService _accountService;

        /// <summary>
        /// Объект Automapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region constructor

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="accountService">Сервис для работы с аккаунтами</param>
        /// <param name="mapper">Объект Automapper</param>
        public AccountController(IAccountService accountService, IMapper mapper)
        {
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this._accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
        }

        #endregion

        #region Api methods

        /// <summary>
        /// Добавить пользователя (регистрация)
        /// </summary>
        /// <param name="saveUser">User object</param>
        /// <returns>Id of the added user</returns>
        [AllowAnonymous]
        [HttpPost("registration")]
        public ActionResult Register([FromBody] RegisterAccountDto registerAccount)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    var account = _mapper.Map<Account>(registerAccount);
                    var result = _accountService.Register(account);
                    return Ok(result);
                }
                catch(AccountValidationException e)
                {
                    return BadRequest(e.Message);
                }
            }
            else
            {
                return BadRequest("Account model is invalid");
            }
        }

        [HttpPost("authentication")]
        public ActionResult Login([FromBody] BaseAccountDto authAccount)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    var account = _mapper.Map<Account>(authAccount);
                    var loggedInAccount = _accountService.Login(account);
                    var result = _mapper.Map<RegisterAccountDto>(loggedInAccount);
                    return Json(result);
                }
                catch(AccountValidationException e)
                {
                    return BadRequest(e.Message);
                }
            }
            else
            {
                return BadRequest("Account model is invalid");
            }
        }

        /// <summary>
        /// Logout user
        /// </summary>
        [HttpDelete("logout/{id:int}")]
        public IActionResult Logout(int id)
        {
            _accountService.Logout(id);
            return Ok();
        }

        #endregion
    }
}