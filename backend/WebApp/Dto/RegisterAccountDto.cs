using System.ComponentModel.DataAnnotations;

namespace WebApp.Dto
{
    public class RegisterAccountDto : BaseAccountDto
    {        

        [Required]
        public string Login
        {
            get;
            set;
        }          
    }
}