using System.ComponentModel.DataAnnotations;

namespace WebApp.Dto
{
    public class BaseAccountDto
    {   
        [Required]
        public string Email
        {
            get;
            set;
        } 

        [Required]
        public string Password
        {
            get;
            set;
        }            
    }
}