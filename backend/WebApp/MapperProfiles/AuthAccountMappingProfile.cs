using AutoMapper;

namespace WebApp.MapperProfiles
{
    public class AuthAccountMappingProfile : Profile
    {
        public AuthAccountMappingProfile()
        {
            CreateMap<WebApp.Dto.BaseAccountDto, Domain.Entity.Account>()
                .ForMember(dst => dst.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dst => dst.Password, opt => opt.MapFrom(src => src.Password));
        }
    }
}