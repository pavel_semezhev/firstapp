using AutoMapper;

namespace WebApp.MapperProfiles
{
    public class RegisterAccountMappingProfile : Profile
    {
        public RegisterAccountMappingProfile()
        {
            CreateMap<WebApp.Dto.RegisterAccountDto, Domain.Entity.Account>()
                .ForMember(dst => dst.Login, opt => opt.MapFrom(src => src.Login))
                .ForMember(dst => dst.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dst => dst.Password, opt => opt.MapFrom(src => src.Password));
        }
    }
}