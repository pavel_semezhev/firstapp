using Microsoft.Extensions.DependencyInjection;
using Dal.EF.Repositories;
using Domain.Entity;
using Domain.Interfaces;

namespace WebApp.Containers
{
    /// <summary>
    /// Class to save repositories
    /// </summary>
    internal class DalEfContainer
    {
        /// <summary>
        /// Method to register repositories and database context
        /// </summary>
        /// <param name="services">collection of services</param>
        /// <param name="connectionString">database connection string</param>
        public static void RegisterRepositories(IServiceCollection services)
        {
            services.AddScoped<IAccountRepository, AccountRepository>();

        }
    }
}
