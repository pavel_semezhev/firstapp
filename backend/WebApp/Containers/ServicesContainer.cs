using Microsoft.Extensions.DependencyInjection;
using Domain.Services.Interfaces;
using Domain.Services;

namespace WebApp.Containers
{
    /// <summary>
    /// Class to save repositories
    /// </summary>
    internal class ServicesContainer
    {
        /// <summary>
        /// Method to register services
        /// </summary>
        /// <param name="services">collection of services</param>
        public static void RegisterServices(IServiceCollection services)
        {
            
            services.AddSingleton<ITokenService, TokenService>();
            services.AddScoped<IAccountService, AccountService>();
        }
    }
}
