using System;
using System.Collections.Generic;

namespace Domain.Interfaces
{
    public interface IBaseRepository<TEntity> : IDisposable
        where TEntity : class
    {
        /// <summary>
        /// Получение всех объектов указанного типа из БД
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Получение объекта указанного типа по Id из БД
        /// </summary>
        /// <returns></returns>
        TEntity GetById(int id);

        /// <summary>
        /// Добавление объекта указанного типа в БД
        /// </summary>
        /// <param name="item"></param>
        void Add(TEntity item);

        /// <summary>
        /// Добавление списка объектов указанного типа в БД
        /// </summary>
        /// <param name="entities"></param>
        void AddRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Изменение объекта указанного типа в БД
        /// </summary>
        /// <param name="item"></param>
        void Update(TEntity item);

        /// <summary>
        /// Сохранение изменений в БД
        /// </summary>
        /// <returns></returns>
        int Save();

        /// <summary>
        /// Удаление записи из БД
        /// </summary>
        /// <param name="entity"></param>
        void Remove(TEntity entity);
    }
}
