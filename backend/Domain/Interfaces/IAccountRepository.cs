using Domain.Entity;

namespace Domain.Interfaces
{
    public interface IAccountRepository : IBaseRepository<Account>
    {
       /// <summary>
       /// Получение объекта Аккаунта по Login
       /// </summary>
       /// <returns></returns>
       Account GetByLogin(string login);

       /// <summary>
       /// Получение объекта Аккаунта по Email
       /// </summary>
       /// <returns></returns>
       Account GetByEmail(string email);
    }
}
