
namespace Domain.Services.Interfaces
{
    public interface ITokenService
    {
        /// <summary>
        /// Добавление токена в словарь
        /// </summary>
        /// <returns></returns>        
        void Add(int id, string token);

        /// <summary>
        /// Удаление токена из словаря
        /// </summary>
        /// <returns></returns>
        void Remove(int id);
    }
}