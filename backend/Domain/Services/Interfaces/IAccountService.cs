using Domain.Entity;
using System.Collections.Generic;
using Domain.Authentication;

namespace Domain.Services.Interfaces
{
    public interface IAccountService
    {
        /// <summary>
        /// Регистрация аккаунта
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        int Register(Account account);

        /// <summary>
        /// Получение аккаунта из БД по Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Account GetById(int accountId);

        /// <summary>
        /// Получение всех аккаунтов в системе
        /// </summary>
        /// <returns></returns>
        IEnumerable<Account> GetAll();

        /// <summary>
        /// Вход в систему
        /// </summary>
        /// <returns></returns>
        AccountAuthResult Login(Account account);

        /// <summary>
        /// Выход из системы
        /// </summary>
        /// <returns></returns>
        void Logout(int id);
    }
}