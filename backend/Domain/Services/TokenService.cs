using Domain.Services.Interfaces;
using System.Collections.Generic;

namespace Domain.Services
{
    public class TokenService : ITokenService
    {

        #region fields and properties

        /// <summary>
        /// Словарь с токенами
        /// </summary>
        /// <returns></returns>        
        private Dictionary<int,string> _tokensDictionary = new Dictionary<int, string>();

        #endregion

        #region ITokenService

        /// <summary>
        /// Добавление токена в словарь
        /// </summary>
        /// <returns></returns>        
        public void Add(int id, string token)
        {   
            _tokensDictionary.Add(id, token);
        }

        /// <summary>
        /// Удаление токена из словаря
        /// </summary>
        /// <returns></returns>
        public void Remove(int id)
        {
            _tokensDictionary.Remove(id);
        }

        #endregion
    }
}