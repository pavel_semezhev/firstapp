using Domain.Entity;
using Domain.Interfaces;
using System.Collections.Generic;
using Domain.Services.Interfaces;
using System.Net.Mail;
using System;
using Domain.Exceptions;
using Domain.Authentication;
using System.Security.Cryptography;
using System.Text;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace Domain.Services
{
    /// <summary>
    /// Service to work with game
    /// </summary>
    public class AccountService : IAccountService
    {
        #region fields and properties

        /// <summary>
        /// Репозиторный класс для работы с аккаунтами в БД
        /// </summary>
        IAccountRepository _accountRepository;

        /// <summary>
        /// Сервис хранения токена
        /// </summary>        
        ITokenService _tokenService;

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="tokenService">Сервис хранения токена</param>
        /// <param name="accountRepository">Репозиторий для работы с объектами аккаунтов в БД</param>
        public AccountService(ITokenService tokenService,IAccountRepository accountRepository)
        {
            this._accountRepository = accountRepository ?? throw new ArgumentNullException(nameof(accountRepository));
            this._tokenService = tokenService ?? throw new ArgumentNullException(nameof(tokenService));
        }

        #endregion

        #region IAccountService
        
        /// <summary>
        /// Регистрация аккаунта
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public int Register(Account account)
        {            
            ValidateAccount(account);
            account.Password = HashPassword(account.Password);
            _accountRepository.Add(account);
            _accountRepository.Save();
            return account.Id;
        }

        /// <summary>
        /// Получение аккаунта из БД по Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Account GetById(int accountId)
        {
            return _accountRepository.GetById(accountId);
        }

        /// <summary>
        /// Получение всех аккаунтов в системе
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Account> GetAll()
        {
            return _accountRepository.GetAll();
        }

        /// <summary>
        /// Вход в систему
        /// </summary>
        /// <returns></returns>
        public AccountAuthResult Login(Account account)
        {
            if(!ValidateEmail(account.Email))
            {
                throw new EmailValidationException(account.Email);
            }
            var existingAccount = GetAccount(account);
            var token = CreateToken(existingAccount);
            AccountAuthResult result = new AccountAuthResult()
            {
                Id = existingAccount.Id,
                Username = existingAccount.Login,
                Email = existingAccount.Email,
                Password = existingAccount.Password,
                Token = token
            };
            _tokenService.Add(existingAccount.Id, token);
            return result;
        }

        /// <summary>
        /// Выход из системы
        /// </summary>
        /// <returns></returns>
        public void Logout(int id)
        {
            _tokenService.Remove(id);
        }

        #endregion

        #region private

        /// <summary>
        /// Валидация полей аккаунта
        /// </summary>
        /// <returns></returns>
        private void ValidateAccount(Account account)
        {
            if(!ValidateEmail(account.Email))
            {
                throw new EmailValidationException(account.Email);
            }           
            if(_accountRepository.GetByLogin(account.Login) != null)
            {
                throw new LoginExistsException(account.Login);
            }
            else if(_accountRepository.GetByEmail(account.Email) != null)
            {
                throw new EmailExistsException(account.Email);
            }            
        }

        /// <summary>
        /// Проверка правильности ввода email'а
        /// </summary>
        /// <returns></returns>       
        private bool ValidateEmail(string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Хэширование пароля
        /// </summary>
        /// <returns></returns> 
        private string HashPassword(string password)
        {
            byte[] tmpSource;
            byte[] tmpHash;
            tmpSource = ASCIIEncoding.ASCII.GetBytes(password);

            tmpHash = new SHA256CryptoServiceProvider().ComputeHash(tmpSource);
            return ByteArrayToString(tmpHash);
        }

        /// <summary>
        /// Преобразование массива байт в строку
        /// </summary>
        /// <returns></returns>      
        private static string ByteArrayToString(byte[] arrInput)
        {
            int i;
            StringBuilder sOutput = new StringBuilder(arrInput.Length);
            for (i=0;i < arrInput.Length; i++)
            {
                sOutput.Append(arrInput[i].ToString("X2"));
            }
            return sOutput.ToString();
        }

        /// <summary>
        /// Получение аккаунта из БД
        /// </summary>
        /// <returns></returns> 
        private Account GetAccount(Account account)
        {
            var existingAccount = _accountRepository.GetByEmail(account.Email);
            if(existingAccount == null)
            {
                throw new EmailNotExistsException();
            }                          
            var password = this.HashPassword(account.Password);
            if(!password.Equals(existingAccount.Password))
            {
                throw new WrongPasswordException();
            }
            return existingAccount;
        }

        /// <summary>
        /// Создание токена
        /// </summary>
        private string CreateToken(Account account)
        {
            var identity = this.GetIdentity(account);
            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }


        /// <summary>
        /// Создание ClaimsIdentity
        /// </summary>
        /// <returns></returns> 
        private ClaimsIdentity GetIdentity(Account account)
        {            
            if (account != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, account.Email),
                    new Claim(ClaimTypes.Name, account.Login)
                };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimTypes.Name);
                return claimsIdentity;
            }
 
            // если пользователя не найдено
            return null;
        }        

        #endregion
    }
}
