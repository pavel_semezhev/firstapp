using System;

namespace Domain.Exceptions
{
    public class AccountValidationException : Exception
    {

        public AccountValidationException()
        {

        }

        public AccountValidationException(string message) : base (message)
        {
        }

    }
}