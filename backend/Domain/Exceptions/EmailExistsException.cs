using System;

namespace Domain.Exceptions
{
    public class EmailExistsException : AccountValidationException
    {
        /// <summary>
        /// Email вызвавший исключение
        /// </summary>
        /// <returns></returns> 
        private readonly string _email;

        /// <summary>
        /// Соообщение
        /// </summary>
        /// <returns></returns>
        public override string Message
        {
            get 
            {
                return $"Account with such email({_email}) already exists";
            }
        }

        public EmailExistsException(string email)
        {
            _email = email;
        }

        public EmailExistsException(string email, string message) : base (message)
        {
            _email = email;
        }
    }
}