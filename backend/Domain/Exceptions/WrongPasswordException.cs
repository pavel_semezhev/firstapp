using System;

namespace Domain.Exceptions
{
    public class WrongPasswordException : AccountValidationException
    {
        /// <summary>
        /// Email вызвавший исключение
        /// </summary>
        /// <returns></returns> 
        private readonly string _email;

        /// <summary>
        /// Соообщение
        /// </summary>
        /// <returns></returns>
        public override string Message
        {
            get 
            {
                return "Password is incorrect";
            }
        }

    }
}