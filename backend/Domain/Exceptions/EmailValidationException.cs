using System;

namespace Domain.Exceptions
{
    public class EmailValidationException : AccountValidationException
    {
        /// <summary>
        /// Email вызвавший исключение
        /// </summary>
        /// <returns></returns>
        private readonly string _email;

        /// <summary>
        /// Соообщение
        /// </summary>
        /// <returns></returns>
        public override string Message
        {
            get 
            {
                return $"Email ({_email}) is invalid";
            }
        }

        public EmailValidationException(string email)
        {
            _email = email;
        }

        public EmailValidationException(string email, string message) : base (message)
        {
            _email = email;
        }
    }
}