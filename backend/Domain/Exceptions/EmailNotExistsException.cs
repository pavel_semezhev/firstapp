using System;

namespace Domain.Exceptions
{
    public class EmailNotExistsException : AccountValidationException
    {
        /// <summary>
        /// Email вызвавший исключение
        /// </summary>
        /// <returns></returns> 
        private readonly string _email;

        /// <summary>
        /// Соообщение
        /// </summary>
        /// <returns></returns>
        public override string Message
        {
            get 
            {
                return "Account with such email doesn't exist";
            }
        }

    }
}