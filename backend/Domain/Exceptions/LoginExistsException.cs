using System;

namespace Domain.Exceptions
{
    public class LoginExistsException : AccountValidationException
    {        
        /// <summary>
        /// Login вызвавший исключение
        /// </summary>
        /// <returns></returns> 
        private readonly string _login;

        /// <summary>
        /// Соообщение
        /// </summary>
        /// <returns></returns>
        public override string Message
        {
            get 
            {
                return $"Account with such login({_login}) already exists";
            }
        }

        public LoginExistsException(string login)
        {
            _login = login;
        }

        public LoginExistsException(string login, string message) : base (message)
        {
            _login = login;
        }
    }
}