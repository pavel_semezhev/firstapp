
namespace Domain.Entity
{
    public class Account
    {

        /// <summary>
        /// Id аккаунта в БД
        /// </summary>
        /// <returns></returns> 
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// Логин аккаунта
        /// </summary>
        /// <returns></returns> 
        public string Login
        {
            get;
            set;
        }

        /// <summary>
        /// Пароль аккаунта
        /// </summary>
        /// <returns></returns> 
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Email аккаунта
        /// </summary>
        /// <returns></returns> 
        public string Email
        {
            get;
            set;
        }          
    }
}