using Microsoft.IdentityModel.Tokens;
using System.Text;
 
namespace Domain.Authentication
{
    public class AuthOptions
    {
        /// <summary>
        /// издатель токена
        /// </summary>
        public const string ISSUER = "WebAppServer"; 
        
        /// <summary>
        /// потребитель токена
        /// </summary>
        public const string AUDIENCE = "WebAppClient";
        
        /// <summary>
        /// ключ для шифрации
        /// </summary>
        const string KEY = "ST6458971546455999745684565688983220132654899753211YuPN";
        
        /// <summary>
        /// время жизни токена сутки
        /// </summary>
        public const int LIFETIME = 1440; 

        /// <summary>
        /// Получить ключ безопасности
        /// </summary>
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
  