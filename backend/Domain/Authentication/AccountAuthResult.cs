
namespace Domain.Authentication
{
    public class AccountAuthResult
    {

        /// <summary>
        /// Id аккаунта в БД
        /// </summary>
        /// <returns></returns> 
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        /// <returns></returns> 
        public string Username
        {
            get;
            set;
        }

        /// <summary>
        /// Пароль аккаунта
        /// </summary>
        /// <returns></returns> 
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Email аккаунта
        /// </summary>
        /// <returns></returns> 
        public string Email
        {
            get;
            set;
        }       

        /// <summary>
        /// Токен аутентификации
        /// </summary>
        /// <returns></returns> 
        public string Token
        {
            get;
            set;
        }    

    }
}