import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Notification, NotificationType} from '@app/core/models';
import {NotificationService} from '@app/core/services';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {
  @Input() id = 'default-notification';
  @Input() fade = true;

  private readonly FADEOUT_TIMEOUT: number = 250;

  notifications: Notification[] = [];
  notificationSubscription: Subscription;
  routeSubscription: Subscription;

  constructor(private router: Router, private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.notificationSubscription = this.notificationService.onAlert(this.id)
      .subscribe(notification => {
        if (!notification.message) {
          this.notifications = this.notifications.filter(x => x.keepAfterRouteChange);
          this.notifications.forEach(x => delete x.keepAfterRouteChange);
          return;
        }
        this.notifications.push(notification);
        if (notification.autoClose) {
          setTimeout(() => this.removeAlert(notification), 3000);
        }
      });

    // clear alerts on location change
    this.routeSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.notificationService.clear(this.id);
      }
    });
  }

  ngOnDestroy(): void {
    this.notificationSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
  }

  removeAlert(notification: Notification): void {
    if (!this.notifications.includes(notification)) {
      return;
    }

    if (this.fade) {
      this.notifications.find(x => x === notification).fade = true;

      setTimeout(() => {
        this.notifications = this.notifications.filter(x => x !== notification);
      }, this.FADEOUT_TIMEOUT);
    } else {
      this.notifications = this.notifications.filter(x => x !== notification);
    }
  }

  cssClass(notification: Notification): string {
    if (!notification) {
      return;
    }

    const classes = ['alert', 'alert-dismissable', 'mt-4', 'container'];

    const notificationTypeClass = {
      [NotificationType.Success]: 'alert alert-success',
      [NotificationType.Error]: 'alert alert-danger',
      [NotificationType.Info]: 'alert alert-info',
      [NotificationType.Warning]: 'alert alert-warning'
    };

    classes.push(notificationTypeClass[notification.type]);

    if (notification.fade) {
      classes.push('fade');
    }

    return classes.join(' ');
  }
}
