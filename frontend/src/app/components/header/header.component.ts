import {Component} from '@angular/core';
import {AccountService} from '@app/core/services';
import {User} from '@app/core/models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  user: User;

  constructor(private accountService: AccountService) {
    this.accountService.user$.subscribe(x => this.user = x);
  }

  logout(): void {
    this.accountService.logout();
  }
}
