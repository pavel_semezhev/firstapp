import {
  HTTP_INTERCEPTORS,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {Injectable} from '@angular/core';
import {delay, dematerialize, materialize, mergeMap} from 'rxjs/operators';

const users = JSON.parse(localStorage.getItem('users')) || [];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const {url, method, headers, body} = req;
    // Эмуляция вызова на бэк
    return of(null)
      .pipe(mergeMap(handleRoute))
      .pipe(materialize()) // https://github.com/Reactive-Extensions/RxJS/issues/648
      .pipe(delay(500))
      .pipe(dematerialize());

    function handleRoute(): Observable<HttpEvent<any>> {
      switch (true) {
        case url.endsWith('/account/registration') && method === 'POST':
          return register();
        case url.endsWith('/account/login') && method === 'POST':
          return authenticate();
        default:
          // пропускаем то что не перехватывается
          return next.handle(req);
      }
    }

    function register(): Observable<HttpResponse<any>> {
      const {user} = body;
      if (users.find(x => x.username === user.username)) {
        return error('Пользователь с именем: "' + user.username + '" уже существует');
      }
      if (users.find(x => x.email === user.email)) {
        return error('Пользователь с email: "' + user.email + '" уже существует');
      }
      user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
      users.push(user);
      localStorage.setItem('users', JSON.stringify(users));
      return ok();
    }

    function authenticate(): Observable<HttpResponse<any>> {
      const {email, password} = body;
      const user = users.find(x => x.email === email);
      if (!user) {
        return error('Пользователь с таким логином и паролем не найден');
      }
      if (user.password !== password) {
        return error('Неверный пароль');
      }
      return ok({
        id: user.id,
        username: user.username,
        token: 'fake-jwt-token'
      });
    }

    function ok(responseBody?: any): Observable<HttpResponse<any>> {
      return of(new HttpResponse({status: 200, body: responseBody}));
    }

    function error(message): Observable<never> {
      return throwError({error: {message}});
    }
  }
}

export const fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
