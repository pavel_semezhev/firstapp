import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {filter} from 'rxjs/operators';
import {Notification, NotificationType} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private subject = new Subject<Notification>();
  private defaultId = 'default-notification';

  onAlert(id = this.defaultId): Observable<Notification> {
    return this.subject.asObservable().pipe(filter(x => x && x.id === id));
  }

  // convenience methods
  success(message: string, options?: any): void {
    this.notification(new Notification({...options, type: NotificationType.Success, message}));
  }

  error(message: string, options?: any): void {
    this.notification(new Notification({...options, type: NotificationType.Error, message}));
  }

  info(message: string, options?: any): void {
    this.notification(new Notification({...options, type: NotificationType.Info, message}));
  }

  warn(message: string, options?: any): void {
    this.notification(new Notification({...options, type: NotificationType.Warning, message}));
  }

  clear(id = this.defaultId): void {
    this.subject.next(new Notification({id}));
  }

  private notification(notification: Notification): void {
    notification.id = notification.id || this.defaultId;
    this.subject.next(notification);
  }
}
