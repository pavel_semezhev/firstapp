import {Injectable} from '@angular/core';
import {User} from '@app/core/models';
import {BehaviorSubject, Observable} from 'rxjs';
import {ApiService} from '@app/core/services/api.service';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private readonly route: string = 'account/';

  private userSubject: BehaviorSubject<User>;
  public user$: Observable<User>;

  get userValue(): User {
    return this.userSubject.value;
  }

  constructor(private apiService: ApiService,
              private router: Router) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user$ = this.userSubject.asObservable();
  }

  register(user: User): Observable<any> {
    return this.apiService.post(this.route + 'registration', {user});
  }

  login(email: string, password: string): Observable<any> {
    return this.apiService.post<User>(this.route + 'login', {email, password})
      .pipe(map(user => {
        localStorage.setItem('user', JSON.stringify(user));
        this.userSubject.next(user);
        return user;
      }));
  }

  logout(): void {
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/account/login']);
  }
}
