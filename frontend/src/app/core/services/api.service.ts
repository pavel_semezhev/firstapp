import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private headers: {};

  private get baseUrl(): string {
    return environment.api.baseUrl;
  }

  constructor(private http: HttpClient) {
  }

  get<T>(route: string, params?: {}): Observable<T> {
    return this.http
      .get<T>(this.baseUrl + route, {
        headers: this.headers,
        params,
      });
  }

  post<T>(
    route: string,
    body: any,
    headers?: { [key: string]: string },
  ): Observable<T> {
    const requestHeaders = {...this.headers, ...(headers || {})};
    return this.http
      .post<T>(this.baseUrl + route, body, {
        headers: requestHeaders,
      });
  }

  put<T>(
    route: string,
    body: any,
    headers?: { [key: string]: string },
  ): Observable<T> {
    const requestHeaders = {
      ...this.headers,
      ...(headers || new HttpHeaders({'Content-Type': 'application/json'})),
    };
    return this.http
      .put<T>(this.baseUrl + route, body, {
        headers: requestHeaders,
      });
  }

  delete(route: string, params?: {}): Observable<object> {
    return this.http
      .delete(this.baseUrl + route, {
        headers: this.headers,
        params,
      });
  }
}
