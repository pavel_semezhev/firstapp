import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountLayoutComponent, AuthorizationComponent, RegistrationComponent} from '@app/modules/account/components';

const routes: Routes = [
  {
    path: '', component: AccountLayoutComponent,
    children: [
      {path: 'login', component: AuthorizationComponent},
      {path: 'registration', component: RegistrationComponent}
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {
}
