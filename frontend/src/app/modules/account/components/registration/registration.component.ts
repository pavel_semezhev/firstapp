import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountService, NotificationService} from '@app/core/services';
import {User} from '@app/core/models';
import {first} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  get formControls(): { [p: string]: AbstractControl } {
    return this.registerForm.controls;
  }

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private accountService: AccountService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit(): void {
    this.submitted = true;
    this.notificationService.clear();
    if (this.registerForm.invalid) {
      return;
    }
    const user: User =
      {...this.registerForm.value};
    this.accountService.register(user).pipe(first())
      .subscribe(
        data => {
          this.notificationService.success('Успешная регистрация', {keepAfterRouteChange: true});
          this.router.navigate(['../login'], {relativeTo: this.route});
        },
        error => {
          this.notificationService.error(error);
          this.loading = false;
        });
  }

}
