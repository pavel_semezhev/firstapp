import {Component, OnInit} from '@angular/core';
import {AccountService} from '@app/core/services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-account-layout',
  templateUrl: './account-layout.component.html',
  styleUrls: ['./account-layout.component.scss']
})
export class AccountLayoutComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute,
              private accountService: AccountService) {
  }

  ngOnInit(): void {
    if (this.accountService.userValue) {
      this.router.navigate(['/']);
    }
    else{
      this.router.navigate(['login'], {relativeTo: this.route});
    }
  }

}
