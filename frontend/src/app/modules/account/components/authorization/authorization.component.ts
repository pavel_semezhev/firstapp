import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService, NotificationService} from '@app/core/services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-authentication',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent implements OnInit {
  authorizationForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  get formControls(): { [p: string]: AbstractControl } {
    return this.authorizationForm.controls;
  }

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private accountService: AccountService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.authorizationForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  onSubmit(): void {
    this.submitted = true;

    this.notificationService.clear();

    if (this.authorizationForm.invalid) {
      return;
    }

    this.loading = true;
    this.accountService.login(this.formControls.email.value, this.formControls.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.notificationService.error(error);
          this.loading = false;
        });
  }
}
