export {RegistrationComponent} from './registration/registration.component';
export {AuthorizationComponent} from './authorization/authorization.component';
export {AccountLayoutComponent} from './account-layout/account-layout.component';
