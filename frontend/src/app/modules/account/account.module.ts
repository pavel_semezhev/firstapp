import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {AccountRoutingModule} from '@app/modules/account/account-routing.module';
import {AccountLayoutComponent, AuthorizationComponent, RegistrationComponent} from '@app/modules/account/components/';

@NgModule({
  declarations: [
    AccountLayoutComponent,
    AuthorizationComponent,
    RegistrationComponent
  ],
  imports: [
    AccountRoutingModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  bootstrap: [AccountLayoutComponent],
  exports: [AccountLayoutComponent],
})
export class AccountModule {
}
